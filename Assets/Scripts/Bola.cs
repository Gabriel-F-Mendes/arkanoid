﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bola : MonoBehaviour
{

	public Vector2 Velocidade;
	
	// Use this for initialization
	void Start ()
	{
		Rigidbody2D body = GetComponent<Rigidbody2D>();
		
		body.AddForce(Velocidade);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
