﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;

public class Jogador : MonoBehaviour
{

	public float Velocidade = 10.0f;
	public float HorizontalAxis;
	public Rigidbody2D rigidybody;
	
	// Use this for initialization
	void Start ()
	{
		rigidybody = GetComponent<Rigidbody2D>();

	}
	
	// Update is called once per frame
	void Update ()
	{
		HorizontalAxis = Input.GetAxis("Horizontal");
		rigidybody.velocity = new Vector2((Velocidade * HorizontalAxis), 0);
	}
}
